require 'yaml'
require 'pry'
require './wiki_parser'

class Logger
  PATH = './config/players.yml'

  def log
    players_by_country.each do |country, players|
      File.open("./output/#{country}.txt", 'a+') do |f|
        players.each_with_index do |player, index|
          puts "processing player #{player}"
          f.puts(index + 1)
          f.puts(parser(player).position)
          f.puts(parser(player).career)
          f.puts('___________________________________')
          f.puts("\n")
        end
      end
    end
  end

  def with_time
    start = Time.now
    log
    finish = Time.now
    puts diff = finish - start
  end

  def players_by_country
    YAML.safe_load(File.read(PATH))
  end

  def parser(player)
    WikiParser.new(name: player)
  end
end

puts Logger.new.with_time
