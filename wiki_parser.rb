require 'nokogiri'
require 'open-uri'
require 'pry'
require 'yaml'

class WikiParser
  attr_reader :name, :cells

  def initialize(name: 'Aleksandr_Filimonov')
    @cells = []
    @name = name
  end

  def position
    data.detect { |p| p.include?('Playing position') }[1]
  end

  def career
    data[index_start + 2..index_end - 1].map { |a| "#{a[1]}(#{a[0]})" }.compact.join(' => ')
  end

  private

  def data
    table.search('tr').each do |tr|
      @cells << tr.search('th, td').map(&:text)
    end

    @cells
  end

  def index_start
    cells.index(['Senior career*'])
  end

  def index_end
    cells.index { |i| i.first.include?('National team') }
  end

  def document
    @document = Nokogiri::HTML(open("https://en.wikipedia.org/wiki/#{name}"))
  end

  def table
    document.at('.infobox.vcard')
  end
end
